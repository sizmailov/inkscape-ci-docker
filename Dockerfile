FROM ubuntu:18.04
ADD install_dependencies.sh /
RUN /install_dependencies.sh --full && rm -rf /install_dependencies.sh /var/lib/apt/lists/*
